<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
echo shell_exec(<<<sh
cd /;
sudo cp /var/sample.conf /etc/apache2/sites-enabled;
sudo mv /etc/apache2/sites-enabled/sample.conf /etc/apache2/sites-enabled/tt.nottes.net.conf;
sudo service apache2 restart;
sh
);
