<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
error_reporting(E_ALL);
ini_set('display_startup_errors', 1);
ini_set('display_errors', '1');
function virtual_host_config($subdomain)
{
    $config_content = file_get_contents('/var/www/sample.conf');
    $config_content = str_replace('domain', $subdomain, $config_content);
    file_put_contents("/var/www/$subdomain.conf", $config_content);
    $sudo_command = <<<sq
sudo mv /var/www/{$subdomain}.conf /etc/apache2/sites-enabled/{$subdomain}.conf;

sq;
    shell_exec($sudo_command);
}
virtual_host_config('rel72.com');

