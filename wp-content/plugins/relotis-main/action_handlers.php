<?php
function relotis_support_page_init()
{
    add_menu_page('Support', 'Support', 'read', 'support', 'relotis_support_page');
}
function relotis_create_site_page_init()
{
    add_menu_page('Create site', 'Create site', 'read', 'create_site', 'relotis_create_site');
}
function relotis_users_control_page_init()
{
    add_menu_page('Relotis Users', 'Relotis Users', 'read', 'relotis_users', 'relotis_users_control');
}
function relotis_plans_page_init()
{
    add_menu_page('Plans', 'Plans', 'read', 'pland', 'relotis_plans_buttons');
}
function keys()
{
    add_menu_page('Relotis Keys', 'Keys', 'read', 'keys', 'relotis_js_keys_file');
}


