/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/

function basic_plan() {
    var stripe = Stripe('base_plan_key');
    var checkoutButton = document.getElementById('checkout-button-plan_HI1E6yuRt7SKkh');
    if (!checkoutButton) return false;
    checkoutButton.addEventListener('click', function () {
        user_plan('base');
        stripe.redirectToCheckout({
            lineItems: [{price: 'plan_HI1E6yuRt7SKkh', quantity: 1}],
            mode: 'subscription',
            successUrl: 'https://relotis.com/success',
            cancelUrl: 'https://relotis.com/canceled',
        })
            .then(function (result) {
                if (result.error) {
                    var displayError = document.getElementById('error-message');
                    displayError.textContent = result.error.message;
                }
            });
    });
}

function growing_plan() {
    var stripe = Stripe('grows_plan_key');
    var checkoutButton = document.getElementById('checkout-button-plan_HI1Fq9lMdW7XQk');
    if (!checkoutButton) return false;
    checkoutButton.addEventListener('click', function () {
        user_plan('growth');
        stripe.redirectToCheckout({
            lineItems: [{price: 'plan_HI1Fq9lMdW7XQk', quantity: 1}],
            mode: 'subscription',
            successUrl: 'https://relotis.com/success',
            cancelUrl: 'https://relotis.com/canceled',
        })
            .then(function (result) {
                if (result.error) {
                    var displayError = document.getElementById('error-message');
                    displayError.textContent = result.error.message;
                }
            });
    });
}
function premium_plan() {
    var stripe = Stripe('premium_plan_key');
    var checkoutButton = document.getElementById('checkout-button-plan_HI1Gi8w64lfACb');
    if (!checkoutButton) return false;
    checkoutButton.addEventListener('click', function () {
        user_plan('premium');
        stripe.redirectToCheckout({
            lineItems: [{price: 'plan_HI1Gi8w64lfACb', quantity: 1}],
            mode: 'subscription',
            successUrl: 'https://relotis.com/success',
            cancelUrl: 'https://relotis.com/canceled',
        })
            .then(function (result) {
                if (result.error) {
                    var displayError = document.getElementById('error-message');
                    displayError.textContent = result.error.message;
                }
            });
    });
}

function user_plan(plan)
{
    $.ajax({
        url: ajaxurl,
        type: 'post',
        data: {
            action: 'save_user_plan',
            plan: plan,
            url: window.location.href
        },
        success: function (resp) {
            $.cookie('plan', plan, { expires: 7, path: '/' });        }
    })
}

$(document).ready(function () {
    setTimeout(function () {
        basic_plan();
        growing_plan();
        premium_plan();

    }, 500);
});
