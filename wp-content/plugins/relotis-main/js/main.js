var is_subdomain,
    subdomain;
function create_site() {
    $('#create_site').click(function () {
        var variant = $('[name=variant]:checked').val();
        if (variant) {
            $.ajax({
                url: ajaxurl,
                type: 'post',
                dataType: 'json',
                data: {
                    action: 'create_site',
                    subdomain: subdomain,
                    is_subdomain: is_subdomain,
                    variant: variant
                },
                beforeSend: function() {
                    ajax_wheel(1);
                },
                success: function (resp) {
                    ajax_wheel(0);
                    if(resp.status === 'ok'){
                        $.ajax({
                            url: ajaxurl,
                            type: 'post',
                            data: {
                                action: 'virtual_host_config',
                                subdomain: subdomain,
                                is_subdomain: is_subdomain,
                            },
                            success: function (resp) {
                                window.location.href = '/wp-admin/admin.php?page=pland';
                            }
                        })
                    }
                    else {
                        console.log(resp);
                        alert('Some error creation of site');
                    }
                }
            })
        }
        else {
            if (!variant) {
                alert('Choose theme for your site');
                return false;
            }
            if (!check_domain) {
                alert('This domain is busy');
                return false;
            }
        }
    });
}
function check_subdomain() {
    $('#check_domain').click(function () {
        is_subdomain = $('[name=is_subdomain]:checked').val();
        if (typeof is_subdomain == 'undefined' || !is_subdomain) {
            $.alert('Please check what you plan to use:<br>your own domain or our subdomain');
            return false;
        }
        if (is_subdomain === 'yes') {
            subdomain = $('input#subdomain').val();
        }
        else {
            subdomain = $('input#domain').val();
        }
        if (!subdomain) {
            $.alert('Enter domain or subdomain please');
            return;
        }
        var pattern = /[^a-z0-9\.\-_]{1,100}/g;
        if (subdomain.match(pattern)){
            $.alert('Please letters in low case, numbers, dot, hyphen, underline only');
            $('#subdomain').val('');
            $('#domain').val('');
            return;
        }
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'check_subdomain',
                subdomain: subdomain
            },
            beforeSend: function() {
                ajax_wheel(1);
            },
            success: function (resp) {
                ajax_wheel(0);
                if (resp === 'fail') {
                    $.alert('This domain is busy, try other');
                    $('#subdomain').val('');
                    $('#domain').val('');
                }
                else {
                    $('#themes').css({display: 'block'});
                    $('#domain').css({display: 'none'});

                }
            }

        })
    })
}

function ajax_wheel(visibility) {
    if (visibility) {
        $('#ajax_gif').css({visibility: 'visible'});
    }
    else {
        $('#ajax_gif').css({visibility: 'hidden'});
    }
}

function choose_theme() {
    $('[name=variant]').change(function () {
        $('#create_site').css({display: 'block'})
    })
}

$(document).ready(function () {
    check_subdomain();
    create_site();
    choose_theme();
});