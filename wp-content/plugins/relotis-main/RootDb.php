<?php
class RootDb{

    /**
     * Принцип: при создании объекта сразу соединяемся с БД, получаем PDO handler
     * и далее его используем для исполнения SQL
     */

    /**
     * Параметры соединения с БД
     */
    private $dbHost = 'localhost';
    private $dbUser = 'root';
    private $dbPass = 'mfo304988kN';
    public $dbName = false;

    /**
     * Идентификатор соединения с БД
     */
    public $handler;

    /**
     * При создании объекта сразу соединяемся с БД, handler потом используется методами класса
     */
    public function __construct(){

        $this->handler = $this->getDbHandler();
        $this->handler->exec('SET NAMES UTF8');
    }

    /**
     * @param  $dbHost
     * @param  $dbName
     * @param  $dbUser
     * @param  $dbPass
     * @return PDO
     */
    private function dbConnect($dbHost, $dbUser, $dbPass) {
        try {
           $dbHandler = new PDO('mysql:host='.$dbHost, $dbUser, $dbPass);
        }
        catch(PDOException $e) {
            echo 'Db error '.$e->getMessage();
            die();
        }

        return $dbHandler;
    }

    /**
     * @return PDO
     */
    private function getDbHandler(){
        $handler = $this->dbConnect($this->dbHost, $this->dbUser, $this->dbPass);
        return $handler;
    }

    /**
     * Подготовка и выполнение SQL-запросов с параметрами, см. примеры
     * @throws Exception
     * @param  $sql
     * @param array $data
     * @return PDOStatement
     */
    public function sqlPrepareAndExecute($sql, $data = []){

        $handler = $this->handler;
        $st = $handler->prepare($sql);// $st - PDOStatement
        $result = $st->execute($data);
        if(!$result){
            throw new Exception('DB error: '.implode(' ', $st->errorInfo()));
        }

        return $st;
    }
}
 
