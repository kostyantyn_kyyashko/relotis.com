<?php
add_action('admin_menu', 'add_scripts');
add_action('admin_menu', 'relotis_create_site_page_init' );
add_action('admin_menu', 'relotis_support_page_init' );
add_action('admin_menu', 'relotis_users_control_page_init' );
add_action('wp_ajax_check_subdomain', 'check_subdomain');
add_action('wp_ajax_create_site', 'create_site');
add_action('wp_ajax_save_user_plan', 'save_user_plan');
add_action('wp_ajax_virtual_host_config', 'virtual_host_config');
add_action('admin_menu', 'relotis_plans_page_init' );
add_action('admin_menu', 'add_stripe_script');
add_action( 'login_enqueue_scripts', 'my_login_logo' );
//add_action( 'init', 'change_href_plan_buttons' );
//add_action( 'init', 'change_href_plan_buttons' );
add_action( "template_redirect", "change_href_plan_buttons" );
add_action('admin_menu', 'keys' );
