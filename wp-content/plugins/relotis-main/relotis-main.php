<?php
/*
Plugin Name: Relotis Main
Description: Relotis auth
Version: 1.0
Author: Relotis
Author URI: http://relotis.com
*/
require_once plugin_dir_path(__FILE__) . 'Db.php';
require_once plugin_dir_path(__FILE__) . 'RootDb.php';
require_once plugin_dir_path(__FILE__) . 'actions.php';
require_once plugin_dir_path(__FILE__) . 'action_handlers.php';


function my_login_logo() { ?>
    <style type="text/css">
        .login form, .login #login_error, .login .message, .login .success {
            border-radius: 15px;
        }
        body.login div#login h1 a {
            background-image: url(/logo.png) !important;
            background-size: auto;
            width: auto;}
        body {
            background: #fff !important;
        }
        .login #backtoblog a, .login #nav a {
            padding: 2px;
            border-radius: 5px;
        }</style>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="  crossorigin="anonymous"></script>
    <script>
            $(document).ready(function () {
                $('#login').find('h1').find('a').attr('href', 'https://relotis.com');
            });
    </script>
<?php }

function change_href_plan_buttons()
{
    ?>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="  crossorigin="anonymous"></script>
    <script src="https://relotis.com/wp-content/plugins/relotis-main/js/jquery.cookie.js"> </script>
    <script>
        var url = window.location.href;
        function get_button_info() {
            $('.elementor-price-table__button').click(function () {
                var href = ($(this).attr('href'));
                var plan = href.split('=')[2];
                $.cookie('plan', plan, { expires: 7, path: '/' });
            })
        }
        if (url === 'https://relotis.com/' || url==='https://relotis.com/pricing/') {
            $(document).ready(function () {
                get_button_info();
            })
        }
    </script>
    <?
}

function relotis_users_control()
{
    global $wpdb;
    $result = $wpdb->get_results('SELECT * FROM user_sites', OBJECT_K );
    ?>
    <table class="table table-bordered table-condensed table-striped table-hover" style="width: auto">
        <thead>
        <th>ID</th>
        <th>WP ID</th>
        <th>Login</th>
        <th>Email</th>
        <th>Domain</th>
        <th>Plan</th>
        <th>Created at</th>
        <th>Block Site</th>
        <th>Remove Site</th>
        </thead>
        <tbody>
        <?foreach ($result as $row):?>
        <tr>
            <td><?=$row->id?></td>
            <td><?=$row->user_id?></td>
            <td><?=$row->user_login?></td>
            <td><?=$row->user_email?></td>
            <td><?=$row->domain?></td>
            <td><?=$row->plan?></td>
            <td><?=date('m/d/Y', $row->created_at)?></td>
            <td style="cursor: pointer;"><i class="far fa-stop-circle"></i></td>
            <td style="cursor: pointer;"><i class="far fa-trash-alt"></i></td>
        </tr>
        <?endforeach?>
        </tbody>
    </table>
    <?
}
function relotis_support_page()
{
    if (isset($_POST['message'])) {
        echo "<h1>Thank you, we will contact you as soon as possible 24 hours</h1>";
        wp_mail('support@relotis.com ', 'Help to ' . $_POST['name'] . '', $_POST['message']);
        return;
    }
    $user = _wp_get_current_user();
    ?>
    <h1>Relotis support:</h1>
    <h2>By email: support@relotis.com</h2>
    <h2>By phone: +1 234 5678912345</h2>
    <h2> Or fill this form</h2>
    <form method="post" action="">
        <label for="user_login"><h3>Your login</h3>
            <input type="text" class="form-control" name="name" value="<?=$user->user_login?>" id="user_login" style="width: 250px;">
        </label>
        <br>
        <label for="user_email"> <h3>Your email</h3>
            <input type="text" class="form-control" name="email" value="<?=$user->user_email?>" id="user_email" style="width: 250px;">
        </label>
        <br>
        <label for="user_message"> <h3>Message to support</h3>
            <textarea class="form-control" name="message" id="user_message" placeholder="Put your message there" style="width: 500px;"></textarea>
        </label>
        <br>
        <button class="btn btn-info" type="submit">Send message to support</button>
    </form>
    <?
}

function add_scripts()
{
    ?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?=plugin_dir_url(__FILE__)?>css/jquery-confirm.less">
    <link rel="stylesheet" href="<?=plugin_dir_url(__FILE__)?>css/jquery-confirm.css">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <script
            src="https://code.jquery.com/jquery-3.5.0.min.js"
            integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous">
    </script>
    <script src="<?=plugin_dir_url(__FILE__)?>js/main.js"></script>
    <script src="<?=plugin_dir_url(__FILE__)?>js/jquery-confirm.js"></script>
    <script src="<?=plugin_dir_url(__FILE__)?>js/plan.js"></script>
    <script src="<?=plugin_dir_url(__FILE__)?>js/jquery.cookie.js"></script>
    <?php
}

function relotis_create_site()
{
    ?>
    <div style="margin: auto; width: 740px;" id="ccontainer">
        <img src="<?=plugin_dir_url(__FILE__)?>img/ajax.gif" id="ajax_gif"
             style="position: absolute; top: 100px; left: 100px; visibility: hidden;">
        <h1>Create your site (step by step)</h1>
        <hr>
        <div id="domain">
            <h2>Step 1</h2>
            <table class="table table-striped table-hover" style="width: auto">
                <tr>
                    <td colspan="3">URL of your site</td>
                </tr>
                <tr>
                    <td colspan="3"><i>If you choose own domain, you need to add (or change) this A-record of domain: @ 161.35.91.133</i></td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <input type="radio" name="is_subdomain" value="no">
                    </td>
                    <td class="align-middle">
                        Your own domain<br>
                    </td>
                    <td>
                        <input type="text" id="domain">
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <input type="radio" name="is_subdomain" value="yes">
                    </td>
                    <td class="align-middle">
                        Our subdomain
                    </td>
                    <td>
                        <input type="text" id="subdomain">.relotis.com
                    </td>
                </tr>
            </table>

                <button class="btn btn-info" id="check_domain">Check and choose this domain</button>
            <hr>
        </div>
        <div style="overflow-y: scroll; height: 500px; display: none" id="themes">
            <h2>Step 2</h2>
            <h2>Choose your site's theme (scroll, view and check one favourite)</h2>
            <table class="table table-condensed table-hover table-striped" style="width: auto;">
                <tbody>
                <?
                for ($i=1; $i<=9; $i++) {
                    ?>
                    <tr>
                        <td class="align-middle"><input name="variant" type="radio" value="<?=$i?>"></td>
                        <td class="align-middle"><a href="http://proto<?=$i?>.relotis.com" target="_blank">Live demo</td>
                        <td><a href="<?=plugin_dir_url(__FILE__)?>img/<?=$i?>.png" target="_blank">
                                <img src="<?=plugin_dir_url(__FILE__)?>img/<?=$i?>mini.png" style="">
                            </a>
                        </td>

                    </tr>
                    <?
                }
                ?>
                </tbody>
            </table>
        </div>
        <hr>
        <div id="create_site" style="display: none;">
            <button class="btn btn-warning" id="create_site" style="font-weight: bold">Create my site!</button>
        </div>

        <p id="new_site_url"></p>
    </div>
    <?
}

function check_subdomain()
{
    $subdomain = $_POST['subdomain'];
    if (dir("/var/www/$subdomain")) {
        echo 'fail';
    }
    else {
        echo 'ok';
    }
}

function create_site()
{
    $subdomain = $_POST['subdomain'];
    $variant = $_POST['variant'];
    $is_subdomain = $_POST['is_subdomain'];
    if ($is_subdomain == 'yes') {
        $domain = "{$subdomain}.relotis.com";
    }
    else {
        $domain = "{$subdomain}";
    }
    setcookie('domain', $domain, time() + 24*3600, '/');
    $status = [
        'url' => '',
        'status' => 'fail',
    ];
    if (!is_dir("/var/www/{$subdomain}")) {
        create_directory($subdomain);
        copy_archive($subdomain, $variant);
        unzip_archive($subdomain);
        copy_htaccess($subdomain, $variant);
        create_db($subdomain);
        create_user_and_config_file($subdomain, $variant);
        fill_dump_into_db($subdomain, $variant);
        change_domain_in_db($subdomain, $variant, $is_subdomain);
        clear_cache($subdomain);
        update_user_in_new_db($subdomain);
        save_user_sites($subdomain, $is_subdomain);
    }
    if ($is_subdomain == 'yes') {
        $status = [
            'url' => "http://$subdomain.relotis.com",
            'status' => 'ok',
        ];
    }
    else {
        $status = [
            'url' => "http://$subdomain",
            'status' => 'ok',
        ];
    }

    echo json_encode($status);
}

function create_directory($subdomain)
{
    return mkdir("/var/www/$subdomain");
}
function copy_archive($subdomain, $variant)
{
    return shell_exec("cp /var/www/proto$variant/files.tar.gz /var/www/$subdomain/files.tar.gz");
}
function unzip_archive($subdomain)
{
    $command = <<<sh
cd /var/www/{$subdomain};
tar xf files.tar.gz
sh;
    return shell_exec($command);
}
function copy_htaccess($subdomain, $variant)
{
    return shell_exec("cp /var/www/proto$variant/.htaccess /var/www/$subdomain/.htaccess");
}

function create_db($subdomain)
{
    $rdb = new RootDb();
    $db = clear_db($subdomain);
    $sql = "CREATE DATABASE {$db}";
    $rdb->sqlPrepareAndExecute($sql);
}
function create_user_and_config_file($subdomain, $variant)
{
    $db = clear_db($subdomain);
    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    $mysql_password = substr(str_shuffle($permitted_chars), 0, 10);
    $rdb = new RootDb();
    $sql = "CREATE USER '{$db}'@'localhost' IDENTIFIED BY '$mysql_password';";
    $rdb->sqlPrepareAndExecute($sql);
    $sql = "GRANT ALL PRIVILEGES ON {$db} . * TO '{$db}'@'localhost';";
    $rdb->sqlPrepareAndExecute($sql);
    $config_content = file_get_contents("/var/www/relotis.com/wp-config-sample.php");
    $config_content = str_replace('qDB_NAME', $db, $config_content);
    $config_content = str_replace('qDB_USER', $db, $config_content);
    $config_content = str_replace('qDB_PASSWORD', $mysql_password, $config_content);
    file_put_contents("/var/www/$subdomain/wp-config.php", $config_content);
}
function fill_dump_into_db($subdomain, $variant)
{
    $DB_USER = 'root';
    $DB_PASS = 'mfo304988kN';
    $EXISTING_DB = 'proto' . $variant;
    $NEW_DB = clear_db($subdomain);
    exec("mysqldump -u".$DB_USER." -p'".$DB_PASS."' ".$EXISTING_DB." | mysql -u ".$DB_USER." --password='".$DB_PASS."' ".$NEW_DB);
}


function change_domain_in_db($subdomain, $variant, $is_subdomain)
{
    if ($is_subdomain == 'yes') {
        $domain = "http://{$subdomain}.relotis.com";
    }
    else {
        $domain = "http://{$subdomain}";
    }
    $rdb = new Db(clear_db($subdomain), 'root', 'mfo304988kN');
    $sql = "UPDATE wp_options SET option_value = REPLACE(option_value, 'http://proto{$variant}.relotis.com', '{$domain}')  WHERE option_name = 'home' OR option_name = 'siteurl';";
    $rdb->sqlPrepareAndExecute($sql);
    $sql = "UPDATE wp_posts SET guid = REPLACE(guid, 'http://proto{$variant}.relotis.com','{$domain}');";
    $rdb->sqlPrepareAndExecute($sql);
    $sql = "UPDATE wp_posts SET post_content = REPLACE(post_content, 'http://proto{$variant}.relotis.com', '{$domain}');";
    $rdb->sqlPrepareAndExecute($sql);
    $sql = "UPDATE wp_postmeta SET meta_value = REPLACE(meta_value,'http://proto{$variant}.relotis.com','{$domain}');";
    $rdb->sqlPrepareAndExecute($sql);
}

function clear_cache($subdomain)
{
    delete_directory("/var/www/$subdomain/wp-content/cache");
}

function delete_directory($dirname) {
    if (is_dir($dirname))
        $dir_handle = opendir($dirname);
    if (!$dir_handle)
        return false;
    while($file = readdir($dir_handle)) {
        if ($file != "." && $file != "..") {
            if (!is_dir($dirname."/".$file))
                unlink($dirname."/".$file);
            else
                delete_directory($dirname.'/'.$file);
        }
    }
    closedir($dir_handle);
    rmdir($dirname);
    return true;
}

function update_user_in_new_db($subdomain)
{
    $user = _wp_get_current_user();
    $sql = <<<sql
UPDATE wp_users 
SET 
user_login = '{$user->data->user_login}',
user_pass = '{$user->data->user_pass}',
user_nicename = '{$user->data->user_nicename}',
user_email = '{$user->data->user_email}',
display_name = '{$user->data->display_name}'
WHERE
ID = 1
sql;
    $db = new Db(clear_db($subdomain), 'root', 'mfo304988kN');
    $db->sqlPrepareAndExecute($sql);
}

function clear_db($subdomain)
{
    return  $db = preg_replace('/[^\w\d]/', '_', $subdomain);
}

function virtual_host_config()
{
    $subdomain = $_POST['subdomain'];
    $is_subdomain = $_POST['is_subdomain'];
    if ($is_subdomain == 'no') {
        $config_content = file_get_contents('/var/www/sample.conf');
        $config_content = str_replace('domain', $subdomain, $config_content);
        file_put_contents("/var/www/$subdomain.conf", $config_content);
        $sudo_command = <<<sq
sudo mv /var/www/{$subdomain}.conf /etc/apache2/sites-enabled/{$subdomain}.conf;
sudo service apache2 restart;
sq;
        shell_exec($sudo_command);
        echo 'ok';
    }
}

function save_user_sites($subdomain, $is_subdomain)
{
    global $wpdb;
    $user = wp_get_current_user();
    if ($is_subdomain == 'yes') {
        $domain = "{$subdomain}.relotis.com";
    }
    else {
        $domain = "{$subdomain}";
    }
    $plan = isset($_COOKIE['plan'])?$_COOKIE['plan']:'';
    $wpdb->insert('user_sites', [
        'user_id' => $user->ID,
        'user_login' => $user->user_login,
        'user_email' => $user->user_email,
        'domain' => $domain,
        'plan' => $plan,
        'created_at' => time(),
    ]);

}
function add_stripe_script()
{
    ?>
    <script src="https://js.stripe.com/v3"></script>
    <?
}


function relotis_plans_buttons()
{
    ?>
    <style>
        .stripe_plan {
            width: 310px;
            height: 600px;
            cursor: pointer;
        }
    </style>
    <table class="table table-hover table-striped table-condensed" style="width: auto; font-size: 150%;">
        <tbody>
        <tr>
            <td style="text-align: right">Your site URL:</td>
            <td style="font-weight: bold;"><a target="_blank" href="http://<?=$_COOKIE['domain']?>"><?=$_COOKIE['domain']?></a></td>
        </tr>
        <tr>
            <td style="text-align: right">Admin Login/Password of your new site</td>
            <td style="font-weight: bold;">Same as relotis.com</td>
        </tr>
        <tr>
            <td style="text-align: right">Your Current Plan</td>
            <td style="font-weight: bold;"><?=isset($_COOKIE['plan'])?$_COOKIE['plan']:'Choose Your Plan' ;?></td>
        </tr>
        </tbody>
    </table>
    <hr>
    <h2 style="margin-top: 10px;">You Can Change / Choose your plan</h2>
    <table>
        <tbody>
        <tr>
            <td class="stripe_plan"><img src="<?=plugin_dir_url(__FILE__)?>img/base.png" id="checkout-button-plan_HI1E6yuRt7SKkh" class="stripe_plan"></td>
            <td class="stripe_plan"><img src="<?=plugin_dir_url(__FILE__)?>img/growth.png" id="checkout-button-plan_HI1Fq9lMdW7XQk" class="stripe_plan"></td>
            <td class="stripe_plan"><img src="<?=plugin_dir_url(__FILE__)?>img/premium.png" id="checkout-button-plan_HI1Gi8w64lfACb" class="stripe_plan"></td>
        </tr>
        </tbody>
    </table>
    <?
}
function save_user_plan()
{
    require_once ABSPATH . '/wp-blog-header.php';
    $domain = $_COOKIE['domain'];
    global $wpdb;
    $plan = $_POST['plan'];
    $user = wp_get_current_user();
    $user_id = $user->ID;
    $wpdb->update('user_sites', ['plan' => $plan], ['user_id' => $user_id, 'domain' => $domain] );
    echo $plan; die();
}

function relotis_js_keys_file() {
    $js_file_path = plugin_dir_path(__FILE__) . "js/plan.js";
    $pattern = "/Stripe(.+)/";
        $js_file_sample_path = plugin_dir_path(__FILE__) . "js/plan_sample.js";
        $sample_js = file_get_contents($js_file_sample_path);
        $sample_js = str_replace('base_plan_key', $_POST['base'], $sample_js);
        $sample_js = str_replace('grows_plan_key', $_POST['grows'], $sample_js);
        $sample_js = str_replace('premium_plan_key', $_POST['premium'], $sample_js);
        if(isset($_POST['base']) || isset($_POST['grows']) || isset($_POST['grows'])) {
            file_put_contents($js_file_path, $sample_js);
        }
    preg_match_all($pattern, file_get_contents($js_file_path), $z);
    $keys = [
        clear_keys($z[0][0]),
        clear_keys($z[0][1]),
        clear_keys($z[0][2]),
    ];
    ?>
    <h2>Your current keys_</h2>
    <h3>Correct/replace keys (if needed) and click "Save keys"</h3>
    <form action="/wp-admin/admin.php?page=keys" method="post">
        <table class="table-bordered, taple-sprited, table-hover">
            <thead>
            <tr>
                <th>Base</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><input class="form-control"type="text" name="base" style="width:400px;" value="<?=$keys[0]?>"</td>
            </tr>
            </tbody>
        </table>
        <hr style="border: 1px solid #dfdfdf;">
        <table class="table-bordered, taple-sprited, table-hover">
            <thead>
            <tr>
                <th>Grows</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><input class="form-control"type="text" name="grows" style="width:400px;" value="<?=$keys[1]?>"</td>
            </tr>
            </tbody>
        </table>
        <table class="table-bordered, taple-sprited, table-hover">
            <thead>
            <tr>
                <th>Premium</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><input class="form-control"type="text" name="premium" style="width:400px;" value="<?=$keys[2]?>"</td>
            </tr>
            </tbody>
        </table>
        <button class="btn btn-info" type="submit" style="margin-top: 10px;">Save keys</button>
    </form>
<?}

function clear_keys($raw_key)
{
    $z =  trim(str_replace("')", '', str_replace("Stripe('", '', $raw_key)));
    $z  = str_replace(';', '', $z);
    return $z;
}