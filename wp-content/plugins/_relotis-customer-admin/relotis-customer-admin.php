<?php
/*
Plugin Name: Relotis Customer Admin
Description: Relotis Customer Admin
Version: 1.0
Author: Relotis
Author URI: http://relotis.com
*/

add_action( 'admin_menu', 'remove_menus' );

function remove_menus(){
    remove_menu_page( 'index.php' );                  //Консоль
    remove_menu_page( 'edit.php' );                   //Записи
    remove_menu_page( 'upload.php' );                 //Медиафайлы
    remove_menu_page( 'edit.php?post_type=page' );    //Страницы
    remove_menu_page( 'edit-comments.php' );          //Комментарии
    remove_menu_page( 'themes.php' );                 //Внешний вид
    remove_menu_page( 'plugins.php' );                //Плагины
    //remove_menu_page( 'users.php' );                  //Пользователи
    remove_menu_page( 'tools.php' );                  //Инструменты
    remove_menu_page( 'options-general.php' );        //Настройки
    remove_menu_page( 'admin.php?page=wpcf7' );        //Настройки
    remove_menu_page( 'admin.php?page=unlimitedelements' );        //Настройки
    remove_menu_page( 'edit.php?post_type=elementor_library&tabs_group=library' );        //Настройки
    ?>
    <style>
        .notice, .woocommerce-layout, #screen-meta-links, #adminmenu, #toplevel_page_piotnet-addons-for-elementor,
        #menu-posts-elementor_library, #toplevel_page_wp-mail-smtp, #toplevel_page_unlimitedelements, #toplevel_page_wc-admin-path--analytics-revenue
        {
            display: none;
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.5.0.min.js"
            integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
            crossorigin="anonymous"></script>

    <script>

        $(document).ready(function () {
            setTimeout(function () {
                //$('.notice').remove();
                $('[href="admin.php?page=wpcf7"]').parent().remove();
                $('[href="admin.php?page=asp_settings"]').parent().remove();
                $('[href="admin.php?page=mailchimp-for-wp"]').parent().remove();
                $('[href="admin.php?page=ai1wm_export"]').parent().remove();
                $('[href="admin.php?page=wc-admin"]').parent().remove();
                $('[href="admin.php?page=wc-admin&amp;path=%2Fanalytics%2Frevenue"]').parent().remove();
                $('[href="edit.php?post_type=product"]').find('.wp-menu-name').text('Listings');
                $('.wp-first-item').find('[href="edit.php?post_type=product"]').text('All listings');
                $('.toplevel_page_elementor').remove();
                $('.menu-posts-elementor_library').remove();
                $('#toplevel_page_wpfastestcacheoptions').remove();
                $('#adminmenu').css({display: 'block'})
                //$('.woocommerce-layout').remove();
                //$('#screen-meta-links').remove();
            }, 50);
        });
    </script>
    <?
    $roles = wp_get_current_user()->roles[0];
    if ($roles != 'administrator') {
        ?>
        <style>
            #toplevel_page_relotis_users {
                display: none;
            }
        </style>
        <?
    }

    if (count($_GET) == 0 && preg_match('/admin/', $_SERVER['REQUEST_URI'])) {
        ?>
        <script>
            //window.location.href = "/wp-admin/edit.php?post_type=product"
        </script>
        <?
    }
}

add_filter( 'gettext', 'change_woocommerce_product_text', 20, 3 );

function change_woocommerce_product_text( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Products' :
            $translated_text = 'Listings';
            break;
        case 'Add new product':
            $translated_text = 'Add new listing';
            break;
        case 'Product categories':
            $translated_text = 'Listing categories';
            break;
        case 'Product tags':
            $translated_text = 'Listing tags';
            break;
    }
    return $translated_text;
}

add_action('admin_menu', 'relotis_support_page_init2' );
function relotis_support_page_init2()
{
    add_menu_page('Support', 'Support', 'read', 'support', 'relotis_support_page2');
}
function relotis_support_page2()
{
    if (isset($_POST['message'])) {
        echo "<h1>Thank you, we will contact you as soon as possible 24 hours</h1>";
        wp_mail('support@relotis.com ', 'Help to ' . $_POST['name'] . '', $_POST['message']);
        return;
    }
    $user = _wp_get_current_user();
    ?>
    <h1>Relotis support:</h1>
    <h2>By email: support@relotis.com</h2>
    <h2>By phone: +1 234 5678912345</h2>
    <h2> Or fill this form</h2>
    <form method="post" action="">
        <label for="user_login"><h3>Your login</h3>
            <input type="text" class="form-control" name="name" value="<?=$user->user_login?>" id="user_login" style="width: 250px;">
        </label>
        <br>
        <label for="user_email"> <h3>Your email</h3>
            <input type="text" class="form-control" name="email" value="<?=$user->user_email?>" id="user_email" style="width: 250px;">
        </label>
        <br>
        <label for="user_message"> <h3>Message to support</h3>
            <textarea class="form-control" name="message" id="user_message" placeholder="Put your message there" style="width: 500px;"></textarea>
        </label>
        <br>
        <button class="btn btn-info" type="submit">Send message to support</button>
    </form>
    <?
}

